;; Getting started with Common Lisp - Tutorial
;; ---
;; Resources: https://lisp-lang.org/learn/first-steps
;;

#|

  Notes
  ---
    - Syntax is based upon S-expressions
    - S-expression is either an atoms (like 10, 2, 42, symbols like t (true) +, variables) or lists
    - Special symbols called keywords like colon-prefixed symbols (like :thing :keyword)
    - Keywords are like enums
|#

;;
;; First steps and functions
;;

(defun say-hello ()
  "Says hello."
  (format t "Hello, Lisp!"))

(defun return-hello ()
  "Returns hello."
  "Hello, Lisp!")

(defun fib (n)
  "Return the n-th Fibonacci number."
  (if (< n 2)
      n
      (+ (fib (- n 1))
         (fib (- n 2)))))

(defun many (n)
  (values n (* n 2) (* n 3)))

;;
;; Variables
;;

(defun local-strings ()
  (let ((str (return-hello)))
    (string-upcase str)))

(defun multiple-vars ()
  (let ((x 1) (y 5))
    (+ x y)))

(defun multiple-dependent-vars ()
  (let* ((x 1) (y (+ x 1)))
    y))

(defun vars-depending-on-args (n)
  (let ((x 1) (y n))
    (+ x y)))

(defun dynamic-variables ()
  (defparameter *string* "> global")
  (defun print-variable ()
    (print *string*))

  (print-variable)

  (let ((*string* ">> dynamically extended!"))
    (print-variable))

  (print-variable))

;;
;; Lists
;;

(defun get-user (n)
  (let ((users (list "John" "Jack" "Jim")))
    (print (nth n users))))

(defun numbers ()
  (let ((numbers (list 1 2 3 4 5 6)))
    (print numbers)
    (print "Altering 3rd number with (setf ...)")
    (setf (nth 3 numbers) 42)
    (print numbers)))

;;
;; Maps
;;

(defun my-map (function list)
  (if list
      (cons (funcall function (first list))
            (my-map function (rest list)))
      nil))

(defun use-my-map (list)
  (if list
      (my-map #'string-upcase list)))

(defun use-mapcar-with-numbers ()
  (mapcar #'evenp (list 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)))

(defun use-mapcar-with-strings ()
  (mapcar #'string-upcase (list "Hello" "Lisp")))

(defun use-mapcar-with-own-function ()
  (defun my-double (n)
    (* n 2))
  (defun call-mapcar-with-own-function ()
    (mapcar #'my-double (list 1 2 3 4)))

  (call-mapcar-with-own-function))

;;
;; Reduce
;;

(defun use-reduce ()
  (reduce #'+ (list 1 2 3 4 5 6 7 8 9 10)))

(defun use-reduce-with-lambda ()
  (reduce #'(lambda (a b)
              (+ a b))
          (list 1 2 3 4 5 6 7 8 9 10)))

(defun understand-reduce (list)
  (reduce #'(lambda (x y)
              ;; ~a seems to be for numbers and ~% for a new line
              (format t "x = ~a, y = ~a and (* ~a ~a) = ~a~%" x y x y (* x y))
              (* x y))
          list))

;;
;; Sorting
;;

(defun use-sort ()
  (sort (list 4 2 6 7 8 3 0 1) #'>))

;;
;; start sbcl REPL with sbcl --load tutorial.lisp
;; ---
;;   (say-hello)
;;
;;   (fib 10) ;; >> 55
;;   (funcall #'fib 10) ;; >> 55
;;   (apply #'fib (list 10)) ;; >> 55
;;
;;   (multiple-value-list (many 2)) ;; >> (2 4 6)
;;   (nth-value 1 (many 2)) ;; >> 4
;;   (multiple-value-bind (first second third)
;;       (many 2)
;;     (list first second third)) ;; >> (2 4 6)
;;
;;   ...
;;
