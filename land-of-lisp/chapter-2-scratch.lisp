;; Land of Lisp - Scratchpad for Lisp Code of Chapter 2

;; Top-level definition
(defparameter *a* 100) ;; mutable
(defvar *b* 200) ;; immutable

(defun output-values (x y)
    (format t "Current values for first var: ~a and second var: ~a~%" x y))

(defun top-level ()
  (output-values *a* *b*)
  (format t "Trying to change both top-level definitions~%")
  (defparameter *a* 101)
  (defvar *b* 202)
  (output-values *a* *b*))

;; Local-level definition
(defun local-level ()
  
  ;; Local variables
  (let ((a 5) (b 10) (c 15))
    (format t "~a~%" (+ a b c)))

  ;; Local functions
  (flet ((f (n) (+ n 10)))
    (format t "~a~%" (f 5)))

  ;; Multiple local functions using flet
  (flet ((f (n)
           (+ n 10))
         (g (n)
           (- n 5)))
    (format t "~a~%" (g (f 5))))

  ;; Multiple local functions using label
  ;; The function b calls function a inside its body, this
  ;; would not be possible with flet (Undefined function A).
  (labels ((a (n)
             (+ n 5))
          (b (n)
             (+ (a n) 6))) ;; function b calls function a!
         (format t "~a~%" (b 10))))
  
