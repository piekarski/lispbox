;; Land of Lisp - Scratchpad for Lisp Code of Chapter 4

;; Empty lists are equal to false
(defun empty-lists ()
  (if '()
      (format t "true")
      (format t "false"))
  (format t "~%")
  (if '(1)
      (format t "true")
      (format t "false")))

;; Simple Recursion eating lists
(defun my-length (list)
  (if list
      (1+ (my-length (cdr list)))
      0))

;; (my-length '(a b c d e f g)) >> 7

;; '() == () == 'nil == nil (that's all the same!, prefer nil and '())
(defun disguises ()
  (eq '() nil)
  (eq '() ())
  (eq '() 'nil)
  (eq nil 'nil))

(defun if1 ()
  (if (= (+ 1 2) (- 4 1))
      (format t "This looks equal!")
      (format t "This is not equal!"))
  (format t "~%"))

(defun if2 ()
  (if (= (* 1 10) (/ 100 100))
      (format t "This looks equal!")
      (format t "This is not equal!"))
  (format t "~%"))

(defun if3 ()
  (if (oddp 5)
      (princ "odd")
      (princ "even")))

(defun if4 ()
  (if (evenp 10)
      (princ "even")))

(defparameter *odd-number* nil)
(defun if5 ()
  (if (oddp 5)
      (progn (setf *odd-number* t)
             (format t "odd~%"))
      (format t "even~%"))
  (princ *odd-number*)
  (setf *odd-number* nil))

(defun when-example ()
  (when (oddp 3)
    (setf *odd-number* t)
    (format t "odd~%"))
  (princ *odd-number*)
  (setf *odd-number* nil))

(defun unless-example ()
  (unless (oddp 4)
    (setf *odd-number* nil)
    (format t "even~%"))
  (princ *odd-number*)
  (setf *odd-number* nil))

(defparameter *n* nil)
(defun cond-example (number)
  (format t "The number ~a is " number)
  (cond ((eq number 5)
         (setf *n* 5)
         (format t "almost right!"))
        ((eq number 7)
         (setf *n* 7)
         (format t "right!"))
        ((eq number 9)
         (setf *n* 9)
         (format t "more or less right"))
        ((oddp number)
         (setf *n* number)
         (format t "at least odd!"))
        (t
         (format t "just wrong!")))
  (format t "~%The final number *n* is ~a~%" *n*)
  (setf *n* nil))

(defvar *arch-enemy* nil)
(defun cond-fun-example (person)
  (cond ((eq person 'henry)
         (setf *arch-enemy* 'stupid-lisp-alien)
         '(curse you lisp alien - you ate my pudding))
        ((eq person 'johnny)
         (setf *arch-enemy* 'useless-old-johnny)
         '(i hope you choked on my pudding johnny))
        (t
         '(why you eat my pudding stranger ?))))

(defun pudding-eater (person)
  (cond-fun-example person))

(defun case-example (number)
  (format t "The number ~a is " number)
  (case number
    ((5)
     (setf *n* 5)
     (format t "almost right!"))
    ((7)
     (setf *n* 7)
     (format t "right!"))
    ((9)
     (setf *n* 9)
     (format t "more or less right"))
    ;; oddp number won't fly with case, skipping
    (otherwise
     (format t "just wrong!")))
  (format t "~%The final number *n* is ~a~%" *n*)
  (setf *n* nil))

(defun case-fun-example (person)
  (case person
      ((henry)
       (setf *arch-enemy* 'stupid-lisp-alien)
       '(curse you lisp alien - you ate my pudding))
      ((johnny)
       (setf *arch-enemy* 'useless-old-johnny)
       '(i hope you choked on my pudding johnny))
      (otherwise
       '(why you eat my pudding stranger ?))))

(defun print-result (result)
  (format t "The result is ~a~%" result))

(defun and-or-examples (number)
  (let ((a (and (evenp number) (> number 10))))
    (print-result a))
  (let ((b (and (oddp number) (< number 10))))
    (print-result b))
  (let ((c (or (evenp number) (oddp number))))
    (print-result c)))

;; Shortcut Boolean Evaluation

(defun shortcut-boo-eval (number)
  (or (oddp number) (format t "Only even numbers will be printed, ~a" number)))

(defun more-than-true ()
  (if (member 1 (list 3 4 1 5))
      (format t "Number 1 is in the list")))

;; Searching for nil

(defun searching-for-nil1 ()
  (let ((result (member nil (list 1 2 nil 4))))
    (princ result)))

(defun searching-for-nil2 ()
  (let ((result (member nil (list 1 2 3 nil))))
    (princ result)))

(defun own-member (n list)
  (cond ((member n list)
         (car (member n list)))))

(defun searching-for-nil3 ()
  (let ((result (own-member nil (list 1 nil 2 3))))
    (princ result)))

(defparameter *found-nil* "nil is inside the list~%")
(defun searching-for-nil-with-member ()
  (if (member nil (list 1 nil 2 3)) ;; this member will return (nil 2 3)
      (format t *found-nil*)))
(defun searching-for-nil-with-own-member ()
  (if (own-member nil (list 1 nil 2 3)) ;; this member will only return nil
      (format t *found-nil*))) ;; though nil is in the list it woun't be reported!

;; Look for even numbers

;; todo: drop amount argument and wrap find-all-even within another function
(defun find-all-even (list amount)
  (cond ((find-if #'evenp list)
         (let ((n (find-if #'evenp list)))
           (format t "~a " n)
           (let ((updated-list (cdr (member n list))))
             (find-all-even updated-list (incf amount)))))
        (t
         (format t "~%That's it, found all ~a even numbers" amount))))

;; Comparing stuff

(defparameter *default-message* "thats no fruit, or I just didn't eat that before")
(defun compare-fruits (fruit)
  (let ((message *default-message*))
    (cond ((eq fruit 'apple)
           (setf message "that's an apple"))
          ((eq fruit 'orange)
           (setf message "that's an orange"))
          ((eq fruit 'cherry)
           (setf message "that's a cherry")))
    (format t "~a - ~a~%" fruit message)))

(defun compare-fruits-alt (fruit)
  (let ((message *default-message*))
    (case fruit
      ((apple) ;; well, no ' needed, why?
       (setf message "looks like an apple"))
      ((orange)
       (setf message "looks like an orange"))
      ((cherry)
       (setf message "looks like a cherry")))
    (format t "~a - ~a~%" fruit message)))

(defparameter *m-eq* "equal")
(defparameter *m-neq* "not equal")
(defparameter *message* "~a and ~a are ~a!~%")
(defun compare-symbols (symbol other-symbol)
  (let ((result ""))
    (if (eq symbol other-symbol)
        (setf result *m-eq*)
        (setf result *m-neq*))
    (format t *message* symbol other-symbol result)))

(defun compare-anything-but-symbols (anything other-anything)
  (let ((result ""))
    (if (equal anything other-anything)
        (setf result *m-eq*)
        (setf result *m-neq*))
    (format t *message* anything other-anything result)))

(defun compare-strings (string other-string)
  (let ((result ""))
    (if (equalp string other-string)
        (setf result *m-eq*)
        (setf result *m-neq*))
    (format t *message* string other-string result)))
