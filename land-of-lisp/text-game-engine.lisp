;; Land of Lisp - Building a text game engine (chapter 5)

;;
;; Locations (nodes) - an association list, an alist
;;

(defparameter *nodes* '((living-room (you are in the living room.
                                      a wizard is snoring loudly on the couch.))
                        (garden (you are in a beatiful garden.
                                 there is a well in front of you.))
                        (attic (you are in the attic.
                                there is a giant welding torch in the corner.))))

(defun describe-location (location nodes)
  "Describe a single location (node).
   Example call: (describe-location 'garden *nodes*)"
  (cadr (assoc location nodes)))

;;
;; Paths (edges between nodes), attic <--> living room <--> garden
;;

(defparameter *edges* '((living-room
                         (garden west door)
                         (attic upstairs ladder))
                        (garden (living-room east door))
                        (attic (living-room downstairs ladder))))

(defun describe-path (edge)
  "Describe a single path (edge).
   Example call: (describe-path '(garden west door))"
  ;; quasi-quoting with a backtick and commata for flip-flopping between data and code modes
  `(there is a ,(caddr edge) going ,(cadr edge) from here.))
;;^            ^                   ^ -- flop to code mode
;;|            |-- flop to code mode
;;|-- flip to data mode

(defun describe-paths (location edges)
  "Describe multiple paths (edges) offered by a single location (edge).
   Example call: (describe-paths 'garden *edges*)"
  ;; find the relevant edges - convert edges to description - join description
  (apply #'append (mapcar #'describe-path (cdr (assoc location edges)))))
;; ^^^^^   ^^^^^^  ^^^^^^ ^^               ^^^ ^^^^^^^^^^^^^^^^^^^^^^
;; |       |       |      |                |   |- assoc looks up edges in location (as key-value pairs)
;; |       |       |      |                |- cdr extracts the value and abandon the key
;; |       |       |      |- shorthand for (function describe-path)
;; |       |       |- mapcar applies a function to every item in a list
;; |       |- append joins several lists into one (concat all edge values)
;; |- apply pretends items in a list are separate
;;    object and passes all to a given function

;;
;; Objects
;;

(defparameter *objects* '(whiskey bucket frog chain))
;;            ^         ^ ^^^...             ...^^^
;;            |         | |- data mode -----------|
;;            |         |- quote mark for changing modes
;;            |- code mode

(defparameter *object-locations* '((whiskey living-room)
                                   (bucket living-room)
                                   (chain garden)
                                   (frog garden)))

(defun objects-at (loc objs obj-locs)
  "Get objects at a location.
   Example call: (objects-at 'garden *objects* *object-locations*)"
  (labels ((at-loc-p (obj) ;; local definition of function at-loc-p (p: predicate)
             (eq (cadr (assoc obj obj-locs)) loc))) ;; return either t or nil objects found at location
    ;; run at-loc-p as a predicate on all objs
    ;; remove all locations for which the at-loc-p returned nil
    (remove-if-not #'at-loc-p objs)))

(defun describe-objects (loc objs obj-loc)
  "Describe objects at a location.
   Example call: (describe-objects 'living-room *objects* *object-locations*)"
  (labels ((describe-obj (obj) ;; local definition of function
             `(you see a ,obj on the floor))) ;; with quasi-quoting create fancy sentence
    ;; find object at current location with objects-at
    ;; map the function describe-obj across all found objects
    ;; append all object descriptions into a single list with apply+append
    (apply #'append (mapcar #'describe-obj (objects-at loc objs obj-loc)))))

;;
;; Look
;;

(defparameter *location* 'living-room) ;; players current location
(defun look ()
  "Take a look around at current *location*.
   Example call: (look)"
  (append (describe-location *location* *nodes*)
          (describe-paths *location* *edges*)
          (describe-objects *location* *objects* *object-locations*)))
                             
;;
;; Walk
;;

(defun walk (direction)
  "Walk into a direction.
   Example call: (walk 'west)
   Valid arguments: '(east west upstairs downstairs)"

  ;; Look up all available paths in *edges* using current *location*
  (let ((next (find direction ;; use find to locate a path by direction (like west, upstairs. etc.)
                    (cdr (assoc *location* *edges*)) ;; get only the values with cdr
                    :key #'cadr))) ;; keyword parameter (name: :key, value: #'cadr)
    ;; as soon as a path is found it is stored in the variable next
    (if next    
        ;; adjust players location and take a look around (if it is a valid direction)
        (progn (setf *location* (car next))
               (look))
        '(you cannot walk that way.))))

;;
;; Pick up
;;

(defun pickup (object)
  "Pick up an object at the current *location*.
   Example call: (pickup 'whiskey)
   Valid arguments: '(whiskey bucket frog chain)"

  ;; Use member function to check if an object is at a location
  ;; The list of available objects is generated with the function objects-at
  (cond ((member object
                 (objects-at *location* *objects* *object-locations*))
         ;; if the object is available at the current *location*
         ;; push a new item to the list *object-locations* key: object and value: players body
         (push (list object 'body) *object-locations*)
         `(you are now carrying the ,object))
        (t '(you cannot pick up that object.))))

;;
;; Inventory
;;

(defun inventory()
  "Show inventory, all objects with the location players body.
   Example call: 
     (pickup whiskey)
     (inventory)"

  ;; retrieves a list of objects with the function objects-at
  ;; where the location is body
  (cons 'items- (objects-at 'body *objects* *object-locations*)))

