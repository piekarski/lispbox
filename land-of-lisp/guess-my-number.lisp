;; Land of Lisp - Guess-my-number Game (p. 20f)

;; Top-level definition (global variables)
;; *name* - * is considered best practice and is called earmuffs but is optional
(defparameter *small* 1)
(defparameter *big* 100)

(defun guess-my-number ()
  ;; arithmetic shift (ash)
  ;;   i.e. (ash (+ 1 100) -1) >> 50
  ;; ash shifts bits either to left or right (where bits at one end get dropped)
  ;;  left shift of number 11 (0x1011) = (ash 11 1) = 22 (0x10110)
  ;;    0x1011 << left shift
  ;;    0x10110
  ;;  right shift of number 11 (0x1011) = (ash 11 -1) = 5 (0x101)
  ;;    0x1011 >> right shift and last bit is dropped
  ;;    0x0101
  (ash (+ *small* *big*) -1))

(defun smaller ()
  (setf *big* (1- (guess-my-number)))
  (guess-my-number))

(defun bigger ()
  (setf *small* (1+ (guess-my-number)))
  (guess-my-number))

(defun start-over ()
  (defparameter *small* 1)
  (defparameter *big* 100)
  (guess-my-number))
