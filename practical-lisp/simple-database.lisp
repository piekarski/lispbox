;;;; Practical Lisp: A simple database

(defun poor-mans-hash-table ()
  "Preliminary function to illustrate a poor mans hash table.
   Example call: (poor-mans-hash-table)"

  (format t "This is an example of a property list accessed with getf.~%")
  (let ((data (list :a 100 :b 200 :c 300 :d 400)))
    (format t "The list data: ")
    (princ data)
    (format t "~%---~%Getting first value with (getf data :b)~%")
    (princ (getf data :b))
    (format t "~%...and another value accessed with (getf data :<key>)~%")
    (princ (getf data :d))))

(defun make-cd (title artist rating ripped)
  "Make a CD.
   Example call: (make-cd 'Nevermind 'Nirvana 4 t)"

  (list :title title :artist artist :rating rating :ripped ripped))

(defvar *db* nil)

(defun add-record (cd)
  "Add a record (cd) to the database *db*.
   Example call: (add-record (make-cd 'Bleach 'Nirvana 6 t))"

  (push cd *db*))

(defun dump-db ()
  "Dump the database in a human-readable format.
   Example call: (dump-db)"

  (dolist (cd *db*) ;; dolist loops over a list *db* and binds every item to cd
    ;; t: shorthand for stdout
    ;; ~{ ... ~} - next consumed argument is a list and all items will be looped over
    ;; ~a - asthetic directive, consumed argument will be output in human-friendly format
    ;; ~Nt - tabulating directive, output N spaces (not tabs!)
    ;; ~% - newline directive, output a new line
    (format t "~{~a:~10t~a~%~}~%" cd)))

(defun dump-db-alt ()
  "Dump the database once more in human-readable format whereas 
   the code itself is a little less human-readable ;-)
   Example call: (dump-db-alt)"

  (format t "~{~{~a:~10t~a~%~}~%~}" *db*)) ;; looping twice with formats ~{...~} directives

(defun prompt-read (prompt)
  "Use *query-io* to prompt and read input with read-line.
   Example call: (prompt-read \"Title\") (ofc without escaping)"

  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-for-cd ()
  "Use prompt-read and make-cd to interactively create a CD.
   Example call: (prompt-for-cd)"

  (make-cd
   (prompt-read "Title")
   (prompt-read "Artist")
   ;; either parse the rating if possible ot get a 0 (and yes any junk record is allowed ;-))
   (or (parse-integer (prompt-read "Rating") :junk-allowed t) 0)
   ;; common function for prompting for a y or n
   ;; (will keep reprompting when it does get anything else)
   (y-or-n-p "Ripped:")))

(defun add-cds ()
  "Add new CDs interactively prompting for every information (title, artist, rating and ripped)
   Example use (add-cds)"

  (loop (add-record (prompt-for-cd)) ;; will loop the body until return is called
        (if (not (y-or-n-p "Another?")) (return))))

(defun save-db (filename)
  "Save the contents of the database *db* to a file going by the name filename.
   Example call: (save-db \"~/cd-db\") -or- (save-db \"C:\cd-db\")"

  ;; with-open-file opens a file for writing (:direction :output), created a file when
  ;; not around (:if-exists :supersede) and binds the stream to the variable out.
  (with-open-file (out filename :direction :output :if-exists :supersede)
    ;; print the database *db* to out with all io options reset to their defaults
    ;; with with-standard-io-syntax (to make sure it'll be readable once written).
    (with-standard-io-syntax
      (print *db* out))))

(defun load-db (filename)
  "Load the content of a file going by the name filename into the database *db*
   Example call: (load-db \"~/cd-db\") -or- (load-db \"C:\cd-db\")"

  ;; with-open-file opens a file for reading and binds the stream to the variable in
  (with-open-file (in filename)
    ;; setf sets the database *db* with the read contents from the stream in
    (with-standard-io-syntax
      (setf *db* (read in)))))

(defun select-by-artist (artist)
  "Select an record(s) by an artist.
  Exammple call: (select-by-artist \"Metallica\")"

  ;; remove-if-not removes one item from a list for which a predicate returns t
  ;; (it does operate on a copy of the list).
  ;; ---
  ;; #'x like #'oddp is a shorthand notation for get me the function with the name
  ;; rather than get me a variable with that name.

  (remove-if-not

   ;; lambda indicates a following definition of a anonymous function
   ;; (it is much like defun only without a name).
   ;; A lambda function is able to derive a part of its meaning from context,
   ;; in this particular case the value of artist.

   #'(lambda (cd) (equal (getf cd :artist) artist))
   *db*))

(defun select (selector-fn)
  "Select an record(s) by a selector function
   Example call: (select #'(lambda (cd) (equal (getf :title) \"Black\")))"

  (remove-if-not selector-fn *db*))

(defun artist-selector (artist)
  "Selector for record(s) by artist
   Example call: (select (artist-selector \"Metallica\"))"

  #'(lambda (cd) (equal (getf cd :artist) artist)))

(defun where (&key title artist rating (ripped nil ripped-p))
  "Magic selector to get record(s) by title, artist, rating and/or ripped.
   Example call: (select (where :rating 5 :ripped t))"

  #'(lambda (cd)
      (and
       (if title    (equal (getf cd :title)  title)  t)
       (if artist   (equal (getf cd :artist) artist) t)
       (if rating   (equal (getf cd :rating) rating) t)
       (if ripped-p (equal (getf cd :ripped) ripped) t))))
